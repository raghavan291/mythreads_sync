/* Single Author info: 
 * rnedunc Raghavendran Nedunchezhian
 * nramanj Navya Ramanjulu
 * Group info:
 * lbalaku Lokheshvar Balakumar
 * rnedunc Raghavendran Nedunchezhian
 * nramanj Navya Ramanjulu */


#include"myatomic.h"

typedef struct mythread_cond_attributes {
        int pshared;
}mythread_condattr_t;

typedef struct mythread_cond_var {
        mythread_condattr_t *attr;
        int  noBlockedThreads;                    /* Number of threads blocked */
        mythread_queue_t blockedThreads;          /* Queue up threads waiting for the condition to become signalled */
        mythread_mutex_t *mutexCond;              /* Mutex that guards access to */
        int stateFlags;				  /* stateFlag is set to 1 to set the least significant bit of the state 
						   * variable in the thread control block to 1*/
}mythread_cond_t;

/*This function is used to initialize the values for cond variable.*/
int mythread_cond_init(mythread_cond_t *cond, const mythread_condattr_t *attr);

/*This function blocks on a condition variable. It atomically releases mutex and causes the calling 
 *thread to block on the condition variable until it is signalled.
*/
int mythread_cond_wait(mythread_cond_t *cond, mythread_mutex_t *mutex);

/*This function unblocks first of the threads that were blocked on the specified condition variable cond*/
int mythread_cond_signal(mythread_cond_t *cond);

/*This function unblocks all threads currently blocked on the specified condition variable cond.*/ 
int mythread_cond_broadcast(mythread_cond_t *cond);

/*This function destroys the condition variable specified by cond. If threads are currently blocked 
 *on the condition variable, this function returns 1
*/
void destroy(mythread_cond_t *cond); 
	
