/* Single Author info: 
 * lbalaku Lokheshvar Balakumar
 * Group info:
 * lbalaku Lokheshvar Balakumar
 * rnedunc Raghavendran Nedunchezhian
 * nramanj Navya Ramanjulu */

#include "mymutex.h"

// This function initializes the mutex variables with default values 

int mythread_mutex_init(mythread_mutex_t *mutex, const mythread_mutexattr_t *attr)
{

	mythread_mutex_t *mx =(mythread_mutex_t *)malloc(sizeof(mythread_mutex_t));
	mx=mutex;
	mx->lockvariable=0;
	mx->noofwaitingthreads=0;
	mx->noofblockedthreads=0;
	mx->isblockedthread=0;
	mx->blocked_threads=(mythread_queue_t * )malloc(sizeof(mythread_queue_t));
	mx->owner_thread=NULL;
	futex_init(&(mutex->blocking_futex),0);
	return 0; // 0  - SUCCESS
}

// This function destroys the mutex with default values. This function doesnt check if the calling threads holds the mutex or not , it just sets the lock variable to an invalid value. Calling this destroy function while the mutex is held by another thread might lead to undefined behaviour

int mythread_mutex_destroy(mythread_mutex_t *mutex)
{
	int result;
	//mythread_mutex_lock(mutex); // obtain the mutex before it is destroyed to make sure that this lock is not used by any other thread
	mythread_enter_kernel();
	mutex->lockvariable=-1;
	mutex->owner_thread=NULL;
	mutex->noofwaitingthreads=0;
        mutex->noofblockedthreads=0;
	mythread_leave_kernel();
	//result= mythread_mutex_unlock(mutex);
	return 0; // SUCCESS
}


// This function performs the function of a TTSL lock with a threshold of 100 unsuccessful attempts to acquire the lock. After 100 unsuccessful attempts this function returns a failure case to the mythread_mutex_lock function and the thread will be blocked until the lock becomes available and no other thread is competing for the lock

int testandtestandset(int *mem)
{
	int numberoftries=100;
	int currenttry=0;
	int oldvalue;

	while(currenttry<numberoftries) //test and test and set lock with a busy wait threshold of 100 times
	{
	
		while(*mem==1) // wait until the lock becomes available
		{
		}

		oldvalue=compare_and_swap(mem,1,0); // try to acquire the lock

		if(oldvalue==0)
		{
			return oldvalue; // if lock acquiring is successful
		}
		else
		{
			currenttry+=1; // if lock acquiring fails then increment the number of unsuccessful attempts 
		}

	}	
	return oldvalue; // if the lock is not acquired after 100 tries, return "FAILURE"  
}

// This function tries to acquire the lock associated with the mutex variable . This function uses the TTSL to acquire the lock. If the lock is not acquired in 100 attempts this function blocks the thread and puts it in a queue until the lock becomes available . This function calls mythread_unblock to notify a blocked thread of the availability of the lock 

int mythread_mutex_lock(mythread_mutex_t *mutex)
{
	int lockstatus;

	mythread_enter_kernel();
	mutex->noofwaitingthreads=mutex->noofwaitingthreads+1;
	mythread_leave_kernel();

	if(mutex->lockvariable==-1)// if the lock was already destroyed then return the error code
	{
		return 1; // error code
	}

	lockstatus=testandtestandset(&(mutex->lockvariable)); // a lockstatus of 0 denotes a successful acquiring of lock 
	
	while(lockstatus==1) // if the lock acquiring fails after 100 tries , then the thread is blocked and put in a queue until lock becomes available
	{
		mythread_enter_kernel();
		mutex->noofblockedthreads=mutex->noofblockedthreads+1;
		mythread_block((mutex->blocked_threads),0);// actual 0
		lockstatus=testandtestandset(&(mutex->lockvariable)); // thread that returns from the block state tries to acquire the lock again
	}

	mythread_enter_kernel();
	mutex->owner_thread=mythread_self();
	mythread_leave_kernel();
	return lockstatus;
}

// This function unlocks the lock variable held by the mutex and also notifies the blocked threads if any in case if there are no other threads that are in busy wait on that lock

int mythread_mutex_unlock(mythread_mutex_t *mutex)
{
	int status;

	if(mutex->lockvariable==-1) // if the lock was already destroyed, return an error code 
	{
	
		return 1; // return the error code
	}

	status=compare_and_swap(&mutex->lockvariable,0,1); // changing the state of the lockvariable to unlocked atomically

	while(status==0)
	{
		status=compare_and_swap(&mutex->lockvariable,0,1);
	}
	
	mythread_enter_kernel();
	mutex->noofwaitingthreads-=1; // decrementing the number of threads waiting on the mutex by one
	mutex->owner_thread=NULL;
	
	// Chech if there are any threads that are blocked in the queue due to exceeding the threshold
	if(mutex->noofwaitingthreads==mutex->noofblockedthreads && mutex->noofwaitingthreads > 0)
	{
		mutex->noofblockedthreads-=1;
		mythread_unblock(mutex->blocked_threads,1);
	}
	else
	{
		mythread_leave_kernel();

	}	
	return 0;
}
