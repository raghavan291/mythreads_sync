/* Single Author info: 
 * rnedunc Raghavendran Nedunchezhian
 * nramanj Navya Ramanjulu
 * Group info:
 * lbalaku Lokheshvar Balakumar
 * rnedunc Raghavendran Nedunchezhian
 * nramanj Navya Ramanjulu */

#include"mymutex.h"
#include"mycond.h"
#include"mythread.h"
#include"myatomic.h"

//default condition variable attributes object
//mythread_condattr_t PTHREAD_COND_INITIALIZER; 

/*This function is used to initialize the values for cond variable.*/
int mythread_cond_init(mythread_cond_t *cond, const mythread_condattr_t *attr)
{
	/*if attr = NULL, set default value.*/
	if(attr == NULL)
	{
		//printf("\nattribute = NULL");
	}	
	cond = (mythread_cond_t *) malloc(sizeof(mythread_cond_t));
	cond->attr = (mythread_condattr_t *) malloc(sizeof(mythread_condattr_t));
	/*if malloc failed, return error code 1 to indicate insufficient memory to create 
	 *condition variable.
	*/
	if(cond == NULL || cond->attr == NULL)
        {
                printf("\nmythread_cond_init - ENOMEM: Insufficient memory exists to create condition variable attributes\n");
                return 1;
        }
	cond->attr = attr;
	cond->mutexCond = (mythread_mutex_t *) malloc(sizeof(mythread_mutex_t));
	mythread_mutex_init(cond->mutexCond, NULL);
	cond->noBlockedThreads = 0;

	/*setting the state flag to 1. This is used to set the last bit of the state variable in the 
         *thread control block to indicate if the thread is in the blocked or unblocked state.
	*/
	cond->stateFlags = 1; 

	/*On completing the initialization successfully return 0.*/
	return 0;
}

/*This function blocks on a condition variable. It should be called with mutex locked by the calling
 *thread or undefined behavior results. This function atomically releases mutex and causes the calling 
 *thread to block on the condition variable until it is signalled.
*/
int mythread_cond_wait(mythread_cond_t *cond, mythread_mutex_t *mutex)
{
	int rc;
	mythread_enter_kernel();
	mythread_t tcb = mythread_self();
	/*Check to see if the calling thread owns the mutex.*/
	if(mutex->owner_thread == tcb)
	{
		/*If the calling thread owns the thread, call block_phase1() to block on the condition 
		 * variable. The 2nd paramter to block_phase1, (which is the stateflag) is set to 1 to 
		 * set the least significant bit of the state variable in the thread control block to 1.
		*/
		mythread_block_phase1(&(cond->blockedThreads), 1); 
		
		/*Increment the no of blocked threads count by 1.*/
		mythread_enter_kernel();
		cond->noBlockedThreads = cond->noBlockedThreads + 1;
		mythread_leave_kernel();
		/*Unlock the mutex so that other threads can acquire it.*/
		rc = mythread_mutex_unlock(mutex);
		/*Check if this thread was signalled already. If it was signalled, the least significant 
		 *bit of the state variable will be set to 0. If LSB = 0, do not enter block_phase2.
		*/
		mythread_enter_kernel();
                if(rc != 0)
                {
                       	return 1;
                }

		if(tcb->state != 0)
		{
			/*Since this thread is not signalled, call block_phase2 and go into blocked state*/
			mythread_block_phase2();
		}
		else
		{
			mythread_leave_kernel();
		}
		
		/*This thread comes out of the blocked state if it is signalled. If so, acquire the lock 
		* (mutex) again.
		*/
		rc = mythread_mutex_lock(mutex);
		/*Upon acquiring the lock successfully, return 0*/
		if(rc == 0)
			return 0;
		else 
			return 1;
	}
	else
	{
	 	mythread_leave_kernel();
	}	
}

/*This function unblocks first of the threads that were blocked on the specified condition variable cond (if 
 *any threads are blocked on cond). 
*/
int mythread_cond_signal(mythread_cond_t *cond)
{
	mythread_enter_kernel();

	/*check if there are any threads blocked on this condition variable cond.*/	
	if(cond->noBlockedThreads > 0)
	{
		/*If so, decrement the no of blocked threads count by 1 and unblock the thread that was 
		 *blocked first.
		*/
		cond->noBlockedThreads = cond->noBlockedThreads - 1;
		mythread_unblock(&(cond->blockedThreads), 1);
	}
	else
	{	
		mythread_leave_kernel();
	}
        //Upon successful completion return 0.
        return 0;
}

/*This function unblocks all threads currently blocked on the specified condition variable cond.*/ 
int mythread_cond_broadcast(mythread_cond_t *cond)
{
	/*while there threads blocked on this condition variable cond, unblock them*/
	while(cond->noBlockedThreads > 0)
	{
		mythread_enter_kernel();
		/*decrement the blocked threads count by 1*/
 		cond->noBlockedThreads = cond->noBlockedThreads - 1;
 		mythread_unblock(&(cond->blockedThreads), 1);
	}

	/*Upon successful completion return 0*/
	return 0;
}

/*This function destroys the condition variable specified by cond. If threads are currently blocked 
 *on the condition variable, this function returns 1
*/
int mythread_cond_destroy(mythread_cond_t *cond) 
{
	//if(cond->mutexCond->owner_thread == mythread_self()) //do we need to check this?
	mythread_enter_kernel();

	if(cond->noBlockedThreads == 0)
	{
		
		cond->stateFlags = 0;
		cond->attr = NULL;
		cond->noBlockedThreads = 0;
		cond->blockedThreads = NULL;	
	}
	else
	{
		printf("\nmythread_cond_destroy - some more threads are blocked, cant destroy cond");
		mythread_leave_kernel();
		return 1;
	}
	mythread_leave_kernel();
	/*Upon successful completion return 0*/
	return 0;
}
