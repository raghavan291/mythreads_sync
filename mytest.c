/* Single Author info: 
 * lbalaku Lokheshvar Balakumar
 * rnedunc Raghavendran Nedunchezhian
 * nramanj Navya Ramanjulu 
 * Group info:
 * lbalaku Lokheshvar Balakumar
 * rnedunc Raghavendran Nedunchezhian
 * nramanj Navya Ramanjulu */

#include <stdio.h>
#include "mymutex.h"
#include "mycond.h"

#define NTHREADS 2

// Global variables used
int conditionMet = 0;
mythread_cond_t cond,cond1;
mythread_mutex_t mutex,mutex1;
int count=0;
int hassignalled=0;

// This is the function that is executed by a set of threads. As each thread enters this function it increments the count variable by 1. If the count variable is less than 25 the threads waits on a condition variable. As the 26th thread comes it broadcasts a signal to all the threads waiting on that particular condition variable. 
//
void * threadfunction1(void *arg)
{
	int i,j;
	i=mythread_mutex_lock(&mutex);
	count++;
	j=count;
	printf("Thread %d Entered.\n",j);
	while(count<25)
	{
	i=mythread_cond_wait(&cond,&mutex);
	}
	printf("Thread %d Exit\n",j); 	

	if(count ==25)
	{
	i=mythread_cond_broadcast(&cond);
	}
	i=mythread_mutex_unlock(&mutex);
}
 
/* this thread function is executed by a thread. It enters the function and waits until it is signalled from the main . */

void * threadfunction2(void *arg)
{
        int i,j;
        i=mythread_mutex_lock(&mutex1);
     	
	printf("************ I am waiting for a signal from the main****************************\n");
        i=mythread_cond_wait(&cond1,&mutex1);
        printf("*********** Got the signal . Leaving .. Bye************************************\n");
        i=mythread_mutex_unlock(&mutex);
}

/* The main first creates a set of 50 threads that will use a mutex and another thread that will use another mutex. The main will wait for the 50 threads to complete. Once the 50 threads are done the main signals the other thread that is waiting on another condition variable */

int main(int argc, char **argv)
{
	mythread_t mythread1[200],mythread2; 
	mythread_mutex_t mutexvar;
	int status,i;
	mythread_setconcurrency(50);
        printf("\nEnter Testcase\n");

	status=mythread_mutex_init(&mutex,NULL);
	status= mythread_cond_init(&cond, NULL);
	
	status=mythread_mutex_init(&mutex1,NULL);
        status= mythread_cond_init(&cond1, NULL);

	printf("mutex initialized %d\n",mutexvar.lockvariable);
	for(i=0;i<50;i++)
	{
		status=mythread_create(&mythread1[i],NULL,&threadfunction1,(void *)&mutexvar);
		if(status != 0)
		{
			printf("\nmythread_create error, rc = %d",status);
		}
	}
	
	status = mythread_create(&mythread1[50],NULL,&threadfunction2,NULL);

	for(i=0;i<50;i++)
	{
		status=mythread_join(mythread1[i],NULL);
	}
	
	
        mythread_cond_destroy(&cond);
        mythread_mutex_destroy(&mutex);
	
	printf("All threads created.. Signalling the waiting thread now\n");
	
	status=mythread_mutex_lock(&mutex1);
	status=mythread_cond_signal(&cond1);
	status = mythread_mutex_unlock(&mutex1);

	mythread_cond_destroy(&cond1);
        mythread_mutex_destroy(&mutex1);
	

        printf("\ntest Main completed\n");
        mythread_exit(NULL);

}
