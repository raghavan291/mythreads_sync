/* Single Author info: 
 * lbalaku Lokheshvar Balakumar
 * Group info:
 * lbalaku Lokheshvar Balakumar
 * rnedunc Raghavendran Nedunchezhian
 * nramanj Navya Ramanjulu */


#include "mythread.h"
#include "myatomic.h"
#include "futex.h"

typedef struct mutexattr
{
	int shared;
	int kind;
}mythread_mutexattr_t;


typedef struct mutex
{

	int lockvariable; // 0 if unlocked 1 if locked
	int noofwaitingthreads; // number of threads that are waiting on the mutex
	int noofblockedthreads; // number of threads that are blocked on the mutex after exceeding the threshold for failure attempts
	int isblockedthread;
	mythread_queue_t *blocked_threads; // queue of threads waiting upon this mutex
	mythread_t owner_thread; // the thread currently holding the lock
	mythread_mutexattr_t *attr; // points to the attribute of the mutex
	struct futex blocking_futex;
	struct mythread_cond_var *currentcondvar;// points to the condition variable that is associated with the mutex at any point of time	
}mythread_mutex_t;

// This function initializes the mutex variables with default values 
int mythread_mutex_init(mythread_mutex_t *mutex, const mythread_mutexattr_t *attr);

// This function tries to acquire the lock associated with the mutex variable . This function uses the TTSL to acquire the lock. If the lock is not acquired in 100 attempts this function blocks the thread and puts it in a queue until the lock becomes available . This function calls mythread_unblock to notify a blocked thread of the availability of the lock
int mythread_mutex_lock(mythread_mutex_t *mutex);

// This function destroys the mutex with default values. This function doesnt check if the calling threads holds the mutex or not , it just sets the lock variable to an invalid value. Calling this destroy function while the mutex is held by another thread might lead to undefined behaviour
int mythread_mutex_destroy(mythread_mutex_t *mutex);

// This function unlocks the lock variable held by the mutex and also notifies the blocked threads if any in case if there are no other threads that are in busy wait on that lock
int mythread_mutex_unlock(mythread_mutex_t *mutex);
