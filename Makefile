CC     = gcc
CFLAGS = -g

mytest: mytest.o mythread.a mymutex.o mycond.o
	$(CC) $(CFLAGS) -o mytest mytest.o mythread.a mymutex.o mycond.o

mymutex.o: mymutex.c
	$(CC) $(CFLAGS) -c mymutex.c -w

mycond.o: mycond.c 
	$(CC) $(CFLAGS) -c mycond.c -w

mytest.o: mytest.c
	$(CC) $(CFLAGS) -c mytest.c -w

clean:
	rm -rf *.o mytest
